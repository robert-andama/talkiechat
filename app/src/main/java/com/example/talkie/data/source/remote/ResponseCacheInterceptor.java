package com.example.talkie.data.source.remote;

import android.content.Context;

import androidx.annotation.NonNull;

import com.example.talkie.utils.AppConstants;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ResponseCacheInterceptor implements Interceptor {
    private Context mContext;
    private static final String TAG = "ResponseCacheIntercepto";

    @NotNull
    @Override
    public okhttp3.Response intercept(@NonNull Chain chain) throws IOException {
        Request request = chain.request();
        if (!Boolean.parseBoolean(request.header("ApplyResponseCache"))) {
            Response originalResponse = chain.proceed(chain.request());
            return originalResponse.newBuilder()
                    .removeHeader("ApplyResponseCache")
                    .header("Cache-Control", "public, max-age=" + AppConstants.MAX_AGE)
                    .build();
        } else {
            return chain.proceed(chain.request());
        }
    }
}