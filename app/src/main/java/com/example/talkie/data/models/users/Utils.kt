package com.example.talkie.data.models.users


import com.example.talkie.data.models.chatList.UserList
import com.pusher.client.channel.User
import org.json.JSONObject

fun User.toUserModel(): UserList {
    val jsonObject1 = JSONObject(this.info)
    val message = jsonObject1.getString("user_info")
    val jsonObject = JSONObject(message)

    val name = jsonObject.getString("name")
    val createdAt = jsonObject.getString("created_at")
    val email = jsonObject.getString("email")
    val emailVerifiedAt = jsonObject.getString("email_verified_at")
    val isReadCount = jsonObject.getInt("is_read_count")
    val messagesCount = jsonObject.getInt("messages_count")
    val unReadCount = jsonObject.getInt("un_read_count")
    val updatedAt = jsonObject.getString("updated_at")
    return UserList(
        this.id.toInt(),
        name,
        email,
        isReadCount,
        unReadCount,
        messagesCount,
        emailVerifiedAt,
        createdAt,
        updatedAt
    )
}