package com.example.talkie.data.source.remote;


import android.content.Context;

import androidx.annotation.NonNull;

import com.example.talkie.utils.AppConstants;
import com.example.talkie.utils.NetworkUtils;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Interceptor to cache data and maintain it for four weeks.
 * <p>
 * If the device is offline, stale (at most four weeks old)
 * response is fetched from the cache.
 */
class OfflineResponseCacheInterceptor implements Interceptor {

    private Context mContext;

    @NotNull
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {

        Request request = chain.request();
        if (Boolean.parseBoolean(request.header("ApplyOfflineCache"))) {
            if (!NetworkUtils.Companion.isNetworkAvailable(mContext)) {
                request = request.newBuilder()
                        .removeHeader("ApplyOfflineCache")
                        .header("Cache-Control", "public, only-if-cached, maxStale = " + AppConstants.MAX_STALE).build();
            }
        }

        return chain.proceed(request);
    }
}