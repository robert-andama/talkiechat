package com.example.talkie.data.source.remote

import com.example.talkie.data.models.chat.Chat
import com.example.talkie.data.models.chatList.UserList
import com.example.talkie.data.models.talkieTokenManeger.TalkieTokenManager
import com.example.talkie.data.models.users.CurrentUser
import io.reactivex.Observable
import retrofit2.http.*

interface TalkieService {

    //  {{TALKIE_BASE_URL}}/oauth/token
    @POST("oauth/token")
    @Headers("No-Authentication:true")
    @FormUrlEncoded
    fun logInTalkieUser(
        @Field("grant_type") grant_type: String,
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("client_secret") client: String,
        @Field("client_id") client_id: String,
        @Field("scope") scope: String
    ): Observable<TalkieTokenManager>


    //  {{TALKIE_BASE_URL}}/oauth/token
    @POST("oauth/token")
    @Headers("No-Authentication:true")
    @FormUrlEncoded
    fun refreshTalkieToken(
        @Field("grant_type") grant_type: String?,
        @Field("refresh_token") username: String?,
        @Field("scope") scope: String?
    ): Observable<TalkieTokenManager?>?


    // {{TALKIE_BASE_URL}}/api/register
    @POST("api/register")
    @Headers("No-Authentication:true")
    @FormUrlEncoded
    fun signUpTalkieUser(
        @Field("name") name: String,
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("password_confirmation") password_confirmation: String
    ): Observable<TalkieTokenManager>


    // {{TALKIE_BASE_URL}}/users/{uuid}
    @GET("api/users")
    fun getLoggedUser(): Observable<CurrentUser>

    // {{TALKIE_BASE_URL}}/chats
    @GET("api/chats")
    fun getUsers(): Observable<List<UserList>>

    // {{TALKIE_BASE_URL}}/chats/{uuid}
    @GET("api/chats/{user}")
    fun getChats(
        @Path("user") userId: Int
    ): Observable<List<Chat>>

    // {{TALKIE_BASE_URL}}/send-chat/{user}
    @POST("api/send-chat/{user}")
    @FormUrlEncoded
    fun postMessage(
        @Path("user") user: Int,
        @Field("message") message: String
    ): Observable<Chat>


}