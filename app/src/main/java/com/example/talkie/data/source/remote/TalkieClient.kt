package com.example.talkie.data.source.remote

import android.content.Context
import com.example.talkie.utils.AppConstants
import com.example.talkie.utils.AppConstants.CACHE_SIZE
import com.example.talkie.utils.AppConstants.TALKIE_BASE_URL
import com.example.talkie.utils.NetworkUtils
import com.example.talkie.utils.PreferenceUtils
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.io.IOException
import java.util.concurrent.TimeUnit

object TalkieClient {

    fun instance(context: Context) {
        mContext = context
    }

    private var token: String? = null
    private var mContext: Context? = null

    private val AuthInterceptor: Interceptor = object : Interceptor {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            var request = chain.request()
            if (!NetworkUtils.isNetworkAvailable(mContext!!)) {
                if (request.header("No-Authentication") == null) {
                    request = request
                        .newBuilder()
                        .header(
                            "Cache-Control",
                            "public, only-if-cached, max-stale=" + AppConstants.MAX_STALE
                        )
                        .addHeader("Authorization", "Bearer $token")
                        .build()
                }
            } else {
                if (request.header("No-Authentication") == null) {
                    request = request.newBuilder()
                        .header("Cache-Control", "public, max-age=" + AppConstants.MAX_AGE)
                        .addHeader("Authorization", "Bearer $token")
                        .build()
                }
            }
            return chain.proceed(request)
        }
    }

    val retrofit: TalkieService by lazy {
        val talkieUserToken: String? = PreferenceUtils.getToken(mContext!!)
        when {
            talkieUserToken != null -> token = talkieUserToken
            else -> PreferenceUtils.disconnectFromTalkie(mContext!!)
        }

        val gson = GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .setLenient()
            .create()

        val cacheFile = File(mContext!!.cacheDir, AppConstants.CACHE_TALKIE_NETWORK_CALLS)
        val cache = Cache(cacheFile, CACHE_SIZE.toLong())
        val client = OkHttpClient.Builder()
            .cache(cache)
            .connectTimeout(
                AppConstants.CACHE_CONNECT_TIMEOUT.toLong(),
                TimeUnit.SECONDS
            )
            .readTimeout(AppConstants.CACHE_READ_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .writeTimeout(AppConstants.CACHE_WRITE_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .addInterceptor(AuthInterceptor)
            .addNetworkInterceptor(ResponseCacheInterceptor())
            .addInterceptor(OfflineResponseCacheInterceptor())
            .build()

        val retrofit: Retrofit = Retrofit.Builder()
            .client(client)
            .baseUrl(TALKIE_BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        retrofit.create(TalkieService::class.java)
    }



}