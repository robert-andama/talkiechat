package com.example.talkie.data.source.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_chats")
class Chats(
    @PrimaryKey @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "message") val message: String,
    @ColumnInfo(name = "from") val from: String,
    @ColumnInfo(name = "to") val to: String,
    @ColumnInfo(name = "is_read") val is_read: String,
    @ColumnInfo(name = "created_at") val created_at: String,
    @ColumnInfo(name = "updated_at") val updated_at: String,
    @ColumnInfo(name = "time_ago") val time_ago: String
)
