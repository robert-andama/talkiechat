package com.example.talkie.data.models.talkieTokenManeger

import android.os.Parcel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class TalkieTokenManager  {
    @SerializedName("token_type")
    @Expose
    var tokenType: String? = null

    @SerializedName("expires_in")
    @Expose
    var expiresIn = 0

    @SerializedName("access_token")
    @Expose
    var accessToken: String? = null

    @SerializedName("refresh_token")
    @Expose
    var refreshToken: String? = null

    protected constructor(`in`: Parcel) {
        tokenType =
            `in`.readValue(String::class.java.classLoader) as String?
        expiresIn = `in`.readValue(Int::class.javaPrimitiveType!!.classLoader) as Int
        accessToken =
            `in`.readValue(String::class.java.classLoader) as String?
        refreshToken =
            `in`.readValue(String::class.java.classLoader) as String?
    }

    /**
     * No args constructor for use in serialization
     *
     */
    constructor() {}

    /**
     *
     * @param tokenType
     * @param accessToken
     * @param expiresIn
     * @param refreshToken
     */
    constructor(
        tokenType: String?,
        expiresIn: Int,
        accessToken: String?,
        refreshToken: String?
    ) : super() {
        this.tokenType = tokenType
        this.expiresIn = expiresIn
        this.accessToken = accessToken
        this.refreshToken = refreshToken
    }

    override fun toString(): String {
        return ""
    }
}