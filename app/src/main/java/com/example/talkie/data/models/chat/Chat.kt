package com.example.talkie.data.models.chat

data class Chat(
    val created_at: String,
    val from: Int,
    val id: Int,
    val is_read: Int,
    val message: String,
    val time_ago: String,
    val to: Int,
    val updated_at: String
)