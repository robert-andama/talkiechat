package com.example.talkie.data.source.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ChatDao {
    @Query("SELECT * from user_chats ORDER BY created_at ASC")
    fun getAll(): List<Chats>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(chats: Chats)

    @Query("DELETE FROM user_chats")
    suspend fun deleteAll()
}