package com.example.talkie.data.models.chatList

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class UserList(
    val id: Int,
    val name: String,
    val email: String,
    @SerializedName("is_read_count") @Expose val isReadCount: Int,
    @SerializedName("un_read_count") @Expose val unReadCount: Int,
    @SerializedName("messages_count") @Expose val messagesCount: Int,
    @SerializedName("email_verified_at") @Expose val emailVerifiedAt: String,
    @SerializedName("created_at") @Expose val createdAt: String,
    @SerializedName("updated_at") @Expose val updatedAt: String,
    var online: Boolean = false
)