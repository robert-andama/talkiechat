package com.example.talkie.data.models.chat

data class ChatMessages(
    val chatMessage: Chat,
    val user: User
)

data class User(
    val created_at: String,
    val email: String,
    val email_verified_at: String,
    val id: Int,
    val name: String,
    val updated_at: String
)