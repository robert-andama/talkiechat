package com.example.talkie.ui.signup

import com.example.talkie.data.models.talkieTokenManeger.TalkieTokenManager
import com.example.talkie.data.source.remote.TalkieService
import com.example.talkie.utils.AppConstants.TALKIE_BASE_URL
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

internal class SignUpPresenter(private var mSignUpView: SignUpView) {

    fun registerUser(
        name: String,
        email: String,
        password: String,
        confirm_password: String
    ) {

        mSignUpView.changeButtonTest()

        // TODO:: add disposables to a bag

        if (mSignUpView.validateInputs()) {
            val subscribe = loadJSON()
                .signUpTalkieUser(name, email, password, confirm_password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { talkieToken: TalkieTokenManager? -> mSignUpView.signUpSuccessful(talkieToken) },
                    { error: Throwable? -> mSignUpView.signUpFailed(error) }
                )
        }


    }

    private fun loadJSON(): TalkieService {
        val client = OkHttpClient.Builder().build()

        val builder: Retrofit.Builder = Retrofit.Builder()
            .baseUrl(TALKIE_BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        val retrofit = builder.build()
        return retrofit.create(TalkieService::class.java)
    }

    fun showChats(talkieToken: TalkieTokenManager) {

        // TODO:: check the server for chat channels, e.g general, presence, private

        // witch user view to the chat activity
        mSignUpView.startChatActivity()

    }

}
