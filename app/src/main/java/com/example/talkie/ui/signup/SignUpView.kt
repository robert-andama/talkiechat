package com.example.talkie.ui.signup

import com.example.talkie.data.models.talkieTokenManeger.TalkieTokenManager

interface SignUpView {

    fun  startChatActivity()
    fun startSignInActivity()
    fun  signUpSuccessful(talkieToken: TalkieTokenManager?)
    fun  signUpFailed(error: Throwable?)
    fun validateInputs(): Boolean
    fun changeButtonTest()

}