package com.example.talkie.ui.chatlist

import com.example.talkie.data.models.chatList.UserList
import com.example.talkie.data.models.users.CurrentUser

interface ChatListView {
    
    fun fetchUsers()
    fun displayUsers(user: List<UserList>)
    fun displayUsersFailed(error: Throwable?)
    fun subscribeToChannel(currentUser: CurrentUser?)
    fun startChatActivity(user: UserList)
    fun getCurrentUser()
    fun saveCurrentUser(currentUser: CurrentUser)

}