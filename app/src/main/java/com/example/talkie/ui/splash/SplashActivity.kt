package com.example.talkie.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import com.example.talkie.R
import com.example.talkie.ui.chatlist.ChatListActivity
import com.example.talkie.ui.login.LoginActivity
import com.example.talkie.utils.AppConstants.SPLASH_DISPLAY_LENGTH
import com.example.talkie.utils.PreferenceUtils

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed(
            { Companion.checkForRegistrationTracking(this) },
            SPLASH_DISPLAY_LENGTH.toLong()
        )

    }

    companion object {
        fun checkForRegistrationTracking(splashActivity: SplashActivity) {

            val registrationLastPoint: String? = PreferenceUtils.readSharedSetting(
                splashActivity,
                splashActivity.getString(R.string.registration_track),
                "false"
            )

            when (registrationLastPoint) {
                "ChatListActivity" -> {
                    registrationLastPoint.javaClass
                    splashActivity.startActivity(Intent(splashActivity, ChatListActivity::class.java))
                    splashActivity.finish()
                }
                else -> {
                    splashActivity.startActivity(Intent(splashActivity, LoginActivity::class.java))
                    splashActivity.finish()
                }
            }


        }
    }
}
