package com.example.talkie.ui.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.talkie.R
import com.example.talkie.data.models.chat.Chat
import com.example.talkie.data.models.chat.ChatMessages
import com.example.talkie.ui.adapter.ChatAdapter
import com.example.talkie.ui.chatlist.ChatListActivity
import com.example.talkie.utils.AppConstants
import com.example.talkie.utils.PreferenceUtils
import com.google.gson.Gson
import com.pusher.client.Pusher
import com.pusher.client.PusherOptions
import com.pusher.client.util.HttpAuthorizer
import com.pusher.pushnotifications.PushNotifications
import kotlinx.android.synthetic.main.activity_chat.*

class ChatActivity : AppCompatActivity(), ChartView {

    private var mChatPresenter: ChatPresenter? = null
    private val mAdapter = ChatAdapter(ArrayList(), this)
    private var userName: String = ""
    private var userEmail: String = ""
    private var userId: Int = 0
    private var chatList = ArrayList<Chat>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        mChatPresenter = ChatPresenter(this)

        fetchUserExtras()
        setupRecyclerView()
        subscribeToChannel()
        getChatMessages()
        setupPusher()
        setupPusherBeams()

        // toolbar
        (this as? AppCompatActivity)?.supportActionBar?.title = userName
        (this as? AppCompatActivity)?.supportActionBar?.subtitle = userEmail
        (this as? AppCompatActivity)?.supportActionBar?.setLogo((R.drawable.bg_rounded_border))

        // send message
        btn_send_message.setOnClickListener {
            mChatPresenter!!.postMessage(userId, et_message.text.toString().trim())
        }
    }

    private fun setupPusherBeams(){
        PushNotifications.start(applicationContext, "ec0b8c59-51a5-435c-874c-9f5c1c44f94c")
        PushNotifications.addDeviceInterest("new-event-message")
    }

    private fun fetchUserExtras() {
        userName = intent.extras?.getString(AppConstants.EXTRA_NAME).toString()
        userEmail = intent.extras?.getString(AppConstants.EXTRA_EMAIL).toString()
        userId = intent.extras?.getInt(AppConstants.EXTRA_ID)!!.toInt()
    }

    private fun setupRecyclerView() {
        with(recyclerViewChat) {
            layoutManager = LinearLayoutManager(this@ChatActivity)
            adapter = mAdapter
        }
    }

    private fun setupPusher() {

        val currentUser = PreferenceUtils.getCurrentUser(this)
        val numbers = listOf(userId, currentUser?.id!!.toInt()).sorted()

        val options = PusherOptions()
        options.setCluster(AppConstants.PUSHER_APP_CLUSTER)
        val pusher = Pusher(AppConstants.PUSHER_APP_KEY, options)
        val channel = pusher.subscribe("talkie-new-message.${numbers[0]}${numbers[1]}")

        channel.bind("new-event-message") { channelName, eventName, data ->
            val gSon = Gson()
            val newMessage = gSon.fromJson(data, ChatMessages::class.java)
            displayUsers(newMessage.chatMessage)
        }

        pusher.connect()
    }


    override fun subscribeToChannel() {
        val token = PreferenceUtils.getToken(this)
        val authorizer = HttpAuthorizer("${AppConstants.TALKIE_BASE_URL}api/pusher/auth/private")

        val authHeader: HashMap<String, String> = HashMap()
        authHeader["Authorization"] = "Bearer $token"
        authHeader["Content-Type"] = "application/x-www-form-urlencoded"
        authHeader["Accept"] = "application/json"
        authorizer.setHeaders(authHeader)

        val options = PusherOptions()
            .setEncrypted(true)
            .setWssPort(443)
            .setAuthorizer(authorizer)
        options.setCluster(AppConstants.PUSHER_APP_CLUSTER)
        val pusher = Pusher(AppConstants.PUSHER_APP_KEY, options)

        mChatPresenter!!.subscribe(pusher, "private-App.User.${userId}")

    }

    override fun displayUsers(chat: Chat) {
        runOnUiThread {
            mAdapter.add(chat)
            chatList.add(chat)
            recyclerViewChat.scrollToPosition(chatList.size - 1);
        }
    }

    override fun displayUsersFailed(error: Throwable) {
        Log.e("ChatRoom error", error.localizedMessage)
    }

    override fun displayChatFailed(error: Throwable) {
        Log.e("Chat error", error.localizedMessage)
    }

    override fun getChatMessages() {
        mChatPresenter?.getChatMessages(userId)
    }

    override fun privateData(data: String) {
        println("private ===========")
        println(data)
        println("private ===========")
    }

    override fun newMessage(message: Chat) {
        Log.e("ChatRoom new", message.message)
    }

    override fun clearEditText() {
        et_message.text?.clear()
    }

    override fun hideKeyBoard() {
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = currentFocus
        if (view == null) view = View(this)
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun validateInputs(): Boolean {
        val email = et_message.text.toString().trim()

        if (email.isEmpty()) {
            Toast.makeText(
                this,
                "please enter a message",
                Toast.LENGTH_LONG
            ).show()

            btn_send_message.isEnabled = true
            return false
        }
        return true
    }

    override fun listChats(messages: List<Chat>) {
        chatList = messages as ArrayList<Chat>
        messages.forEach(mAdapter::add)
        recyclerViewChat.scrollToPosition(messages.size - 1);
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> {
                PreferenceUtils.disconnectFromTalkie(this)
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this, ChatListActivity::class.java))
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        mChatPresenter?.dispose()
    }

}
