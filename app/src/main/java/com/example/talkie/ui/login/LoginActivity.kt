package com.example.talkie.ui.login


import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.talkie.R
import com.example.talkie.data.models.talkieTokenManeger.TalkieTokenManager
import com.example.talkie.ui.chatlist.ChatListActivity
import com.example.talkie.ui.signup.SignUpActivity
import com.example.talkie.utils.PreferenceUtils
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), LoginView {

    private var mLoginPresenter: LoginPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        button_auth_sign_in.setOnClickListener {
            val email = input_email.text.toString()
            val password = input_password.text.toString()

            mLoginPresenter!!.logInUser(email, password)
        }
        button_create_room.setOnClickListener {}
        button_existing_room!!.setOnClickListener {}

        mLoginPresenter = LoginPresenter(this)
    }

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun authSuccessful(talkieToken: TalkieTokenManager?) {
        // save token to pref
        saveToSharedPreference(talkieToken!!)
        // login user and show chat activity
        mLoginPresenter!!.joinChat("anonymous")
    }


    override fun changeButtonText() {
        button_auth_sign_in!!.text = getString(R.string.text_waiting_for_auth)
        button_auth_sign_in.isEnabled = false
    }

    override fun startChatActivity(roomName: String) {
        startActivity(Intent(this, ChatListActivity::class.java))
        finish()
    }

    override fun signUpFailed(error: Throwable?) {
        button_auth_sign_in.isEnabled = true
        button_auth_sign_in.text = getText(R.string.authentication_label)

        error!!.message?.let { showToast(it) }

    }

    override fun validateInputs(): Boolean {
        val email = input_email.text.toString()
        val password = input_password.text.toString()

        if (email.isEmpty() || password.isEmpty()) {
            Toast.makeText(
                this,
                "One or more fields are required please fill them",
                Toast.LENGTH_LONG
            ).show()

            button_auth_sign_in.isEnabled = true
            button_auth_sign_in.text = getText(R.string.authentication_label_sign_up)
            return false
        }
        return true
    }

    fun startSignUpActivity(view: View) {
        startActivity(Intent(this, SignUpActivity::class.java))
    }


    private fun saveToSharedPreference(talkieToken: TalkieTokenManager) {

        val token = Gson().toJson(talkieToken)
        PreferenceUtils.saveSharedSetting(
            applicationContext,
            getString(R.string.talkie_user_token_management),
            token
        )

        PreferenceUtils.saveSharedSetting(
            applicationContext,
            getString(R.string.talkie_token),
            talkieToken.accessToken
        )

        PreferenceUtils.saveSharedSetting(
            this,
            this.getString(R.string.registration_track),
            "ChatListActivity"
        )

    }


}
