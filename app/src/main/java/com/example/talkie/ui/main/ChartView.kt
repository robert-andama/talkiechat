package com.example.talkie.ui.main

import com.example.talkie.data.models.chat.Chat

interface ChartView {

    fun subscribeToChannel()
    fun displayUsers(chat: Chat)
    fun newMessage(message: Chat)
    fun clearEditText()
    fun hideKeyBoard()
    fun validateInputs(): Boolean
    fun listChats(messages: List<Chat>)
    fun displayUsersFailed(error: Throwable)
    fun displayChatFailed(error: Throwable)
    fun getChatMessages()
    fun privateData(data: String)


}