package com.example.talkie.ui.main

import android.util.Log
import com.example.talkie.data.models.chat.Chat
import com.example.talkie.data.source.remote.TalkieClient
import com.pusher.client.Pusher
import com.pusher.client.channel.PrivateChannelEventListener
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

internal class ChatPresenter(private val mChatView: ChartView) {

    private val disposables = CompositeDisposable()


    fun subscribe(pusher: Pusher, nameOfChannel: String) {
        Log.i("ChatRoom", nameOfChannel)

        pusher.connect()

        pusher.subscribePrivate(nameOfChannel, object : PrivateChannelEventListener {
            override fun onEvent(channelName: String?, eventName: String?, data: String) {
                mChatView.privateData(data)
            }

            override fun onAuthenticationFailure(p0: String?, p1: Exception?) {
                Log.e("ChatRoom", p1!!.localizedMessage)
            }

            override fun onSubscriptionSucceeded(p0: String?) {
                Log.i("ChatRoom", "Successful subscription")
            }

        }, "TalkiePrivateChannel")
    }

    fun getChatMessages(userId: Int) {

        val subscribe =
            TalkieClient.retrofit
                .getChats(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { messages: List<Chat> -> mChatView.listChats(messages) },
                    { error: Throwable -> mChatView.displayChatFailed(error) }
                )
        disposables.add(subscribe)
    }

    fun postMessage(
        userId: Int,
        newMessage: String
    ) {

        if (mChatView.validateInputs()) {

            val subscribe =
                TalkieClient.retrofit
                    .postMessage(userId, newMessage)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        { message: Chat -> mChatView.newMessage(message) },
                        { error: Throwable -> mChatView.displayUsersFailed(error) }
                    )

            mChatView.clearEditText()
            mChatView.hideKeyBoard()

            disposables.add(subscribe)
        }

    }

    fun dispose() {
        if (!disposables.isDisposed) {
            disposables.clear()
        }
    }
}