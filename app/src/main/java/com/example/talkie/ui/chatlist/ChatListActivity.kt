package com.example.talkie.ui.chatlist

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.talkie.R
import com.example.talkie.data.models.chatList.UserList
import com.example.talkie.data.models.users.CurrentUser
import com.example.talkie.data.models.users.toUserModel
import com.example.talkie.data.source.remote.TalkieClient
import com.example.talkie.ui.adapter.UserRecyclerAdapter
import com.example.talkie.ui.main.ChatActivity
import com.example.talkie.utils.AppConstants
import com.example.talkie.utils.PreferenceUtils
import com.google.gson.Gson
import com.pusher.client.Pusher
import com.pusher.client.PusherOptions
import com.pusher.client.channel.PresenceChannelEventListener
import com.pusher.client.channel.User
import com.pusher.client.util.HttpAuthorizer
import kotlinx.android.synthetic.main.activity_chat_list.*
import org.json.JSONObject

class ChatListActivity : AppCompatActivity(), ChatListView, UserRecyclerAdapter.UserClickListener {

    private var mChatListPresenter: ChatListPresenter? = null
    private val mAdapter = UserRecyclerAdapter(ArrayList(), this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_list)

        mChatListPresenter = ChatListPresenter(this)
        getCurrentUser()
        setupRecyclerView()
    }

    override fun onStart() {
        super.onStart()
        fetchUsers()
        subscribeToChannel(PreferenceUtils.getCurrentUser(this))
    }

    private fun setupRecyclerView() {
        with(recyclerViewUserList) {
            layoutManager = LinearLayoutManager(this@ChatListActivity)
            adapter = mAdapter
        }
    }

    override fun fetchUsers() {
        TalkieClient.instance(this)
        mChatListPresenter!!.getUsers()
    }

    override fun displayUsers(userList: List<UserList>) {
        userList.forEach {
            this.mAdapter.add(it, this)
        }
    }

    override fun displayUsersFailed(error: Throwable?) {
        Toast.makeText(this, error!!.localizedMessage, Toast.LENGTH_LONG).show()
    }

    override fun subscribeToChannel(currentUser: CurrentUser?) {

        val token = PreferenceUtils.getToken(this)
        val authorizer = HttpAuthorizer("${AppConstants.TALKIE_BASE_URL}api/pusher/auth/presence")

        val authHeader: HashMap<String, String> = HashMap()
        authHeader["Authorization"] = "Bearer $token"
        authHeader["Content-Type"] = "application/x-www-form-urlencoded"
        authHeader["Accept"] = "application/json"
        authorizer.setHeaders(authHeader)

        val options = PusherOptions()
            .setEncrypted(true)
            .setWssPort(443)
            .setAuthorizer(authorizer)
        options.setCluster(AppConstants.PUSHER_APP_CLUSTER)
        val pusher = Pusher(AppConstants.PUSHER_APP_KEY, options)
        pusher.connect()

        pusher.subscribePresence("presence-talkie-room",
            object : PresenceChannelEventListener {
                override fun onUsersInformationReceived(p0: String?, users: MutableSet<User>?) {
                    for (user in users!!) {
                        if (user.id != PreferenceUtils.getCurrentUser(this@ChatListActivity)?.id.toString()) {
                            runOnUiThread {
                                mAdapter.showUserOnline(user.toUserModel())
                            }
                        }
                    }
                }

                override fun onEvent(p0: String?, p1: String?, p2: String?) {
                    Log.d("onEvent1", p0);
                    Log.d("onEvent2", p1);
                    Log.d("onEvent3", p2);
                }

                override fun onAuthenticationFailure(p0: String?, p1: Exception?) {
                    Log.e("Pusher", p1!!.message)
                }

                override fun onSubscriptionSucceeded(p0: String?) {
                    Log.i("Pusher", "Subscription succeeded")
                }

                override fun userSubscribed(channelName: String, user: User) {
                    runOnUiThread {
                        mAdapter.showUserOnline(user.toUserModel())
                    }
                }

                override fun userUnsubscribed(channelName: String, user: User) {
                    runOnUiThread {
                        mAdapter.showUserOffline(user.toUserModel())
                    }
                }
            })


    }

    override fun startChatActivity(user: UserList) {
    }

    override fun getCurrentUser() {
        TalkieClient.instance(this)
        mChatListPresenter!!.getCurrentUser()
    }

    override fun saveCurrentUser(currentUser: CurrentUser) {
        val currentUserToJson = Gson().toJson(currentUser, CurrentUser::class.java)
        PreferenceUtils.saveSharedSetting(this, getString(R.string.current_user), currentUserToJson)
    }

    override fun onUserClicked(user: UserList) {
        val intent = Intent(this, ChatActivity::class.java)
        intent.putExtra(AppConstants.EXTRA_ID, user.id)
        intent.putExtra(AppConstants.EXTRA_NAME, user.name)
        intent.putExtra(AppConstants.EXTRA_EMAIL, user.email)
        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        mChatListPresenter?.dispose()
    }
}
