package com.example.talkie.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.talkie.R
import com.example.talkie.data.models.chat.Chat
import com.example.talkie.ui.main.ChatActivity
import com.example.talkie.utils.AppConstants.TYPE_MESSAGE_RECEIVED
import com.example.talkie.utils.AppConstants.TYPE_MESSAGE_SENT
import com.example.talkie.utils.PreferenceUtils
import kotlinx.android.synthetic.main.item_message_received.view.*
import kotlinx.android.synthetic.main.item_message_sent.view.*
import java.util.*

class ChatAdapter(
    private var list: ArrayList<Chat>,
    var mContext: Context
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val viewHolder: RecyclerView.ViewHolder
        val view: View

        if (viewType == TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_message_received, parent, false);
            viewHolder = ReceivedMessageViewHolder(view)
        } else {
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_message_sent, parent, false);
            viewHolder = SentMessageViewHolder(view)
        }

        return (viewHolder);
    }

    override fun getItemViewType(position: Int): Int {
        val currentUser = PreferenceUtils.getCurrentUser(mContext)

        return if (list[position].from == currentUser?.id) {
            TYPE_MESSAGE_SENT
        } else TYPE_MESSAGE_RECEIVED
    }

    override fun getItemCount(): Int = list.size

    fun add(message: Chat) {
        list.add(message)
        notifyDataSetChanged()
    }


    inner class ReceivedMessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(message: Chat) = with(itemView) {
            tv_receive_username?.text = message.message
            tv_receive_message?.text = message.time_ago
        }
    }

    inner class SentMessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(message: Chat) = with(itemView) {

            tv_sent_username?.text = message.message
            tv_sent_message?.text = message.time_ago
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val chatMessage: Chat = list[position]

        val currentUser = PreferenceUtils.getCurrentUser(mContext)

        if (chatMessage.to == currentUser?.id) {
            (holder as ReceivedMessageViewHolder).bind(chatMessage)
        } else {
            (holder as SentMessageViewHolder).bind(chatMessage)
        }
    }
}