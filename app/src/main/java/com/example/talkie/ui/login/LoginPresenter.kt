package com.example.talkie.ui.login

import com.example.talkie.data.models.talkieTokenManeger.TalkieTokenManager
import com.example.talkie.data.source.remote.TalkieService
import com.example.talkie.utils.AppConstants
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class LoginPresenter(private var mILoginView: LoginView) {

    fun logInUser(email: String, password: String) {

        mILoginView.changeButtonText()

        if (mILoginView.validateInputs()) {
            val client = "DMcNwMTxxkPq20Q02dWk4ezNkakHFZDEhJ7bTvT4"
            val clientId = "6"
            val subscribe = loadJSON()
                .logInTalkieUser("password", email, password, client, clientId, "*")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { talkieToken: TalkieTokenManager? -> mILoginView.authSuccessful(talkieToken) },
                    { error: Throwable? -> mILoginView.signUpFailed(error) }
                )
        }
    }

    private fun loadJSON(): TalkieService {
        val client = OkHttpClient.Builder().build()

        val builder: Retrofit.Builder = Retrofit.Builder()
            .baseUrl(AppConstants.TALKIE_BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        val retrofit = builder.build()
        return retrofit.create(TalkieService::class.java)
    }

    fun joinChat(roomName: String) {
        mILoginView.startChatActivity(roomName)
    }

}
