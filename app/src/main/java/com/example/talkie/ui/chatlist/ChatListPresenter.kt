package com.example.talkie.ui.chatlist

import com.example.talkie.data.models.chatList.UserList
import com.example.talkie.data.models.users.CurrentUser
import com.example.talkie.data.source.remote.TalkieClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ChatListPresenter (private var mChatListView: ChatListView) {

    private val disposables = CompositeDisposable()


    fun getUsers(){
        val subscribe: Disposable = TalkieClient.retrofit
            .getUsers()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { userList: List<UserList> ->   mChatListView.displayUsers(userList)  },
                { error: Throwable -> mChatListView.displayUsersFailed(error) }
            )
        disposables.add(subscribe)

    }

    fun getCurrentUser() {
        val subscribe: Disposable = TalkieClient.retrofit
            .getLoggedUser()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { currentUser: CurrentUser ->   mChatListView.saveCurrentUser(currentUser)  },
                { error: Throwable -> mChatListView.displayUsersFailed(error) }
            )

        disposables.add(subscribe)

    }

    fun dispose() {
        if (!disposables.isDisposed) {
            disposables.clear()
        }
    }

}