package com.example.talkie.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.talkie.R
import com.example.talkie.data.models.chatList.UserList
import com.example.talkie.ui.chatlist.ChatListActivity
import com.example.talkie.utils.PreferenceUtils
import kotlinx.android.synthetic.main.user_list_row.view.*
import java.util.*

class UserRecyclerAdapter(
    private var list: ArrayList<UserList>,
    private var listener: UserClickListener
) : RecyclerView.Adapter<UserRecyclerAdapter.UserViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.user_list_row, parent, false)
        )
    }

    /*
    * Add new users joining
    * */
    fun add(
        user: UserList,
        chatListActivity: ChatListActivity
    ) {
        val currentUser = PreferenceUtils.getCurrentUser(chatListActivity)
        if (user.id != currentUser?.id) list.add(user)
        notifyDataSetChanged()
    }


    inner class UserViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        fun bind(currentValue: UserList) = with(itemView) {

            this.setOnClickListener { listener.onUserClicked(currentValue) }

            txt_username.text = currentValue.name
            txt_last_message.text = currentValue.email
            if (currentValue.online) img_presence.setImageDrawable(
                ContextCompat.getDrawable(
                    this.context,
                    R.drawable.online
                )
            ) else img_presence.setImageDrawable(
                ContextCompat.getDrawable(
                    this.context,
                    R.drawable.offline
                )
            )
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind(list[position])
    }

    interface UserClickListener {
        fun onUserClicked(user: UserList)
    }

    fun showUserOnline(updatedUser: UserList) {
        list.forEachIndexed { index, element ->
            if (updatedUser.id == element.id) {
                updatedUser.online = true
                list[index] = updatedUser
                notifyItemChanged(index)
            }

        }
    }

    fun showUserOffline(updatedUser: UserList) {
        list.forEachIndexed { index, element ->
            if (updatedUser.id == element.id) {
                updatedUser.online = false
                list[index] = updatedUser
                notifyItemChanged(index)
            }
        }
    }

}
