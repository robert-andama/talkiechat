package com.example.talkie.ui.signup

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.talkie.R
import com.example.talkie.data.models.talkieTokenManeger.TalkieTokenManager
import com.example.talkie.ui.chatlist.ChatListActivity
import com.example.talkie.ui.login.LoginActivity
import com.example.talkie.ui.main.ChatActivity
import com.example.talkie.utils.PreferenceUtils
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_signup.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException

class SignUpActivity : AppCompatActivity(), SignUpView {

    private var mSignUpPresenter: SignUpPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        mSignUpPresenter = SignUpPresenter(this)

        txt_sign_in.setOnClickListener { startSignInActivity() }

        button_auth_sign_up.setOnClickListener {
            val name = input_name.text.toString()
            val email = input_email.text.toString()
            val password = input_password.text.toString()

            mSignUpPresenter!!.registerUser(
                name,
                email,
                password,
                password
            )
        }

    }

    override fun startChatActivity() {
        startActivity(Intent(this, ChatListActivity::class.java))
        finish()
    }


    override fun startSignInActivity() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    override fun signUpSuccessful(talkieToken: TalkieTokenManager?) {
        // save token to pref
        saveToSharedPreference(talkieToken!!)
        // login user and show chat activity
        mSignUpPresenter!!.showChats(talkieToken)
    }

    override fun signUpFailed(error: Throwable?) {
        button_auth_sign_up.isEnabled = true
        button_auth_sign_up.text = getText(R.string.authentication_label_sign_up)

        if (error is SocketTimeoutException) {
            val message = "We are having trouble connection to the server. Please try again."
            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
            return
        }
        if (error is IOException) {
            val message = "There was an Error loading Data. Please try again."
            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
            return
        }

        if (error is RuntimeException) {
            try {
                val exception = error as HttpException
                val errorBody: String?
                val invalidFields: String
                try {
                    errorBody = exception.response()!!.errorBody()!!.string()
                    val err = JSONObject(errorBody)
                    if (err.has("invalid_fields")) {
                        invalidFields = err.getString("invalid_fields")
                        val invalidCredentials = JSONObject(invalidFields)
                        if (invalidCredentials.has("email")) {
                            textInputLayout5.error = invalidCredentials.getString("email")
                        }
                        if (invalidCredentials.has("password")) {
                            textInputLayout4.error = invalidCredentials.getString("password")
                        }
                    } else {
                        Log.e("onLoginFailed: %s", exception.toString())
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                Log.e("In onError() %s", exception.toString())
            } catch (ex: ClassCastException) {
                Log.e("onLoginFailed: %s", ex.toString())
                Log.e("onLoginFailed: %s", error.toString())
            }
        }
        Log.e("err", error.toString())
    }

    private fun saveToSharedPreference(talkieToken: TalkieTokenManager) {

        val token = Gson().toJson(talkieToken)
        PreferenceUtils.saveSharedSetting(
            applicationContext,
            getString(R.string.talkie_user_token_management),
            token
        )

        PreferenceUtils.saveSharedSetting(
            applicationContext,
            getString(R.string.talkie_token),
            talkieToken.accessToken
        )

        PreferenceUtils.saveSharedSetting(
            this,
            this.getString(R.string.registration_track),
            "ChatListActivity"
        )
    }

    override fun validateInputs(): Boolean {
        val name = input_name.text.toString()
        val email = input_email.text.toString()
        val password = input_password.text.toString()

        if (name.isEmpty() || email.isEmpty() || password.isEmpty()) {
            Toast.makeText(
                this,
                "One or more fields are required please fill them",
                Toast.LENGTH_LONG
            ).show()

            button_auth_sign_up.isEnabled = true
            button_auth_sign_up.text = getText(R.string.authentication_label_sign_up)
            return false
        }

        return  true
    }

    override fun changeButtonTest() {
        button_auth_sign_up.text = getString(R.string.sign_up_process_notce)
        button_auth_sign_up.isEnabled = false
    }
}
