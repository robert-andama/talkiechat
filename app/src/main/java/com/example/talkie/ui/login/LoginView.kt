package com.example.talkie.ui.login

import com.example.talkie.data.models.talkieTokenManeger.TalkieTokenManager


interface LoginView {
    fun showToast(message: String)
    fun authSuccessful(talkieToken: TalkieTokenManager?)
    fun changeButtonText()
    fun startChatActivity(roomName: String)
    fun signUpFailed(error: Throwable?)
    fun validateInputs(): Boolean
}
