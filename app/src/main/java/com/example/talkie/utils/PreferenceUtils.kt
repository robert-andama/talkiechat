package com.example.talkie.utils

import android.content.Context
import android.content.Intent
import com.example.talkie.R
import com.example.talkie.data.models.talkieTokenManeger.TalkieTokenManager
import com.example.talkie.data.models.users.CurrentUser
import com.example.talkie.ui.login.LoginActivity
import com.example.talkie.utils.AppConstants.PREFERENCES_FILE
import com.google.gson.Gson

class PreferenceUtils {


    companion object {
        fun readSharedSetting(
            ctx: Context,
            settingName: String,
            defaultValue: String
        ): String? {
            val sharedPref = ctx.getSharedPreferences(
                PREFERENCES_FILE,
                Context.MODE_PRIVATE
            )
            return sharedPref.getString(settingName, defaultValue)
        }

        fun saveSharedSetting(
            ctx: Context,
            settingName: String?,
            settingValue: String?
        ) {
            val sharedPref = ctx.getSharedPreferences(
                PREFERENCES_FILE,
                Context.MODE_PRIVATE
            )
            val editor = sharedPref.edit()
            editor.putString(settingName, settingValue)
            editor.apply()
        }

        fun getToken(mContext: Context): String? {
            return readSharedSetting(
                mContext,
                mContext.getString(R.string.talkie_token),
                "null"
            )
        }

        fun getCurrentUser(mContext: Context): CurrentUser? {
            val currentUser = readSharedSetting(
                mContext,
                mContext.getString(R.string.current_user),
                "null"
            )

            val gSon = Gson()
            return gSon.fromJson(currentUser, CurrentUser::class.java)
        }

        fun getTalkieToken(mContext: Context): TalkieTokenManager? {
            val talkieUserCompanyManagement = readSharedSetting(
                mContext,
                mContext.getString(R.string.talkie_user_token_management),
                "null"
            )

            val gSon = Gson()
            return gSon.fromJson(
                talkieUserCompanyManagement,
                TalkieTokenManager::class.java
            )
        }

        fun disconnectFromTalkie(mContext: Context) {
            saveSharedSetting(mContext, mContext.getString(R.string.registration_track), "")
            saveSharedSetting(mContext, mContext.getString(R.string.talkie_token), "null")
            saveSharedSetting(mContext, mContext.getString(R.string.talkie_user_token_management), null)
            saveSharedSetting(mContext, mContext.getString(R.string.current_user), null)

            mContext.startActivity(Intent(mContext, LoginActivity::class.java))
        }
    }
}