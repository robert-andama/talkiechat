package com.example.talkie.utils


object AppConstants {
    const val SPLASH_DISPLAY_LENGTH = 1000

    const val PREFERENCES_FILE = "talkie_settings"

    // cache
    const val CACHE_SIZE = 10 * 1024 * 1024
    const val CACHE_CONNECT_TIMEOUT = 60
    const val CACHE_READ_TIMEOUT = 60
    const val CACHE_WRITE_TIMEOUT = 60
    const val CACHE_TALKIE_NETWORK_CALLS = "talkie_network_calls"
    const val MAX_STALE = 60 * 60 * 24 * 28 // tolerate 4-weeks stale
    const val MAX_AGE = 0

    const val TYPE_MESSAGE_SENT = 393
    const val TYPE_MESSAGE_RECEIVED = 529

    const val EXTRA_ID: String = "id"
    const val EXTRA_NAME: String = "name"
    const val EXTRA_EMAIL: String = "email"


    const val PUSHER_APP_KEY: String = "e9a6cc2b0f3cf8a0e980"
    const val PUSHER_APP_CLUSTER: String = "mt1"

//    const val TALKIE_BASE_URL = "http://talkie.tgmgospels.com/"
    const val TALKIE_BASE_URL = "https://jengavitu.com/"

}